#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include <ctype.h>
#include <stdio.h>
#include <limits.h>

#include <OpenIPMI/ipmi_conn.h>
#include <OpenIPMI/ipmi_addr.h>
#include <OpenIPMI/ipmi_err.h>
#include <OpenIPMI/ipmi_msgbits.h>
#include <OpenIPMI/ipmi_picmg.h>

#include <OpenIPMI/internal/ipmi_oem.h>
#include <OpenIPMI/internal/ipmi_domain.h>
#include <OpenIPMI/internal/ipmi_mc.h>
#include <OpenIPMI/internal/ipmi_int.h>
#include <OpenIPMI/internal/ipmi_event.h>
#include <OpenIPMI/internal/ipmi_sel.h>

//#include <OpenIPMI/internal/ipmi_sensor.h>
//#include <OpenIPMI/internal/ipmi_control.h>
//#include <OpenIPMI/internal/ipmi_entity.h>
//#include <OpenIPMI/internal/ipmi_utils.h>
//#include <OpenIPMI/internal/ipmi_fru.h>

static int utca_get_hot_swap_state(ipmi_entity_t *entity, ipmi_entity_hot_swap_state_cb handler, void *cb_data) ;
static int utca_check_hot_swap_state(ipmi_entity_t *entity) ;
static int utca_deactivate(ipmi_entity_t  *entity, ipmi_entity_cb done, void *cb_data) ;
static int utca_activate(ipmi_entity_t  *entity, ipmi_entity_cb done, void *cb_data) ;

static int utca_initialized = 0 ;

typedef struct utca_fru_s
{
    unsigned int fru_id;
    ipmi_entity_t *entity;
    enum ipmi_hot_swap_states hs_state;
    ipmi_sensor_id_t hs_sensor_id;
    unsigned char hs_sensor_lun;
    unsigned char hs_sensor_num;

    unsigned char presence_check_in_progress ;
} utca_fru_t;

typedef struct utca_hs_info_s
{
    ipmi_entity_hot_swap_state_cb handler1;
    ipmi_entity_cb handler2;
    void *cb_data;
    ipmi_entity_op_info_t sdata;
    ipmi_sensor_op_info_t sdata2;
    utca_fru_t *finfo;
    int op;
} utca_hs_info_t;

static enum ipmi_hot_swap_states utca_hs_to_openipmi[] =
{
    IPMI_HOT_SWAP_NOT_PRESENT,
    IPMI_HOT_SWAP_INACTIVE,
    IPMI_HOT_SWAP_ACTIVATION_REQUESTED,
    IPMI_HOT_SWAP_ACTIVATION_IN_PROGRESS,
    IPMI_HOT_SWAP_ACTIVE,
    IPMI_HOT_SWAP_DEACTIVATION_REQUESTED,
    IPMI_HOT_SWAP_DEACTIVATION_IN_PROGRESS,
    IPMI_HOT_SWAP_OUT_OF_CON,
};

static ipmi_entity_hot_swap_t utca_hot_swap_handlers =
{
    .get_hot_swap_state = utca_get_hot_swap_state,

    .set_auto_activate = NULL,
    .get_auto_activate = NULL,
    .set_auto_deactivate = NULL,
    .get_auto_deactivate = NULL,

    .set_activation_requested = NULL, //atca_unlock_fru,
    .activate = utca_activate,
    .deactivate = utca_deactivate,

    .get_hot_swap_indicator = NULL, //atca_get_hot_swap_indicator,
    .set_hot_swap_indicator = NULL, //atca_set_hot_swap_indicator,
    .get_hot_swap_requester = NULL, //atca_get_hot_swap_requester,

    .check_hot_swap_state = utca_check_hot_swap_state,
};



void utca_sel_new_event_handler_cb(ipmi_sel_info_t *sel, ipmi_mc_t *mc, ipmi_event_t *event, void *cb_data)
{
// printf("this is new event handler\n") ;
}

static void utca_event_handler(ipmi_domain_t *domain, ipmi_event_t *event, void *event_data)
{
// the main purpose is to rescan system when new board appears - not to detect changes of board states
    unsigned char data[13];
    unsigned char old_state;
    unsigned char new_state;
    unsigned char sensor_type;

// ipmi_mc_t *mc;
//
    if (ipmi_event_get_type(event) != 2)
    {
        printf("Not a system event\n") ;
        return;
    }

    ipmi_event_get_data(event, data, 0, 13);
    if (data[6] != 4)
    {
        printf("Not IPMI 1.5\n") ;
        return;
    }

    if (ipmi_event_is_old(event))
    {
        printf("It's an old event, ignore it.\n") ;
        return;
    }


    sensor_type = data[7];


    {
        old_state = data[11] & 0xf;
        new_state = data[10] & 0xf;

//printf("utca event handler %X %d %d %d\n", sensor_type, old_state, new_state, data[12]) ;
    }

    switch(sensor_type)
    {
        case 0xf0:
        {
            old_state = data[11] & 0xf;
            new_state = data[10] & 0xf;
// printf("state change of %d: %d -> %d\n",data[12],old_state,new_state) ;
            break;
        }

// this is ATCA code - ours must be different
// if ((old_state == 0) || (new_state == 0))
// if (data[12] != 0) {
// /* FRU id is not 0, it's an AMC module (or something else the
// IPMC manages). If the device has gone away or is newly
// inserted, rescan the SDRs on the IPMC. */
// ipmi_ipmb_addr_t addr;
// ipmi_mc_t *mc;
//
// addr.addr_type = IPMI_IPMB_ADDR_TYPE;
// addr.channel = data[5] >> 4;
// addr.slave_addr = data[4];
// addr.lun = 0;
//
// mc = i_ipmi_find_mc_by_addr(domain, (ipmi_addr_t *) &addr,
// sizeof(addr));
// if (mc) {
// ipmi_mc_reread_sensors(mc, NULL, NULL);
// i_ipmi_mc_put(mc);
// }
// }
//else {
// /* We have a hot-swap event on the main where the previous
// state was not installed. Scan the MC to make it appear.
// Note that we always do this, in case we missed a hot-swap
// removal and this is a changed MC. */
// ipmi_start_ipmb_mc_scan(domain, (data[5] >> 4) & 0xf,
// data[4], data[4], NULL, NULL);
// }

        default:
        {
            break ;
        }
    }


// case IPMI_SENSOR_TYPE_VERSION_CHANGE:
// if ((data[10] != 1) && (data[10] != 7))
// break;
// mc = i_ipmi_event_get_generating_mc(domain, NULL, event);
// if (!mc)
// break;
// ipmi_mc_reread_sensors(mc, NULL, NULL);
// i_ipmi_mc_put(mc);
// /* FIXME - what about FRU data? */
// break;
// }
}


static void
hot_swap_checker(ipmi_entity_t             *entity,
		 int                       err,
		 enum ipmi_hot_swap_states state,
		 void                      *cb_data)
{
    utca_fru_t                *finfo = NULL ;
    enum ipmi_hot_swap_states old_state;
    int                       handled = IPMI_EVENT_NOT_HANDLED;
    ipmi_event_t              *event = NULL;

    finfo = ipmi_entity_get_oem_info(entity);

    if ( finfo == NULL )
    {
      // we got uninitialized entity - lets just abort check
      return ;
    }

    if (err)
    {
      if ( finfo->presence_check_in_progress == 1 )
      {
         ipmi_entity_detector_done(entity, (finfo->hs_state == IPMI_HOT_SWAP_NOT_PRESENT)?0:1) ;
         finfo->presence_check_in_progress = 0 ;
      }

      return;
    }

    if (state != finfo->hs_state) {
	old_state = finfo->hs_state;
	finfo->hs_state = state;
	ipmi_entity_call_hot_swap_handlers(entity, old_state, state, &event,
					   &handled);
    }

    // if we are during presence check we can notify
    if ( finfo->presence_check_in_progress == 1 )
    {
       ipmi_entity_detector_done(entity, (finfo->hs_state == IPMI_HOT_SWAP_NOT_PRESENT)?0:1) ;
       finfo->presence_check_in_progress = 0 ;
    }
}

static int utca_check_hot_swap_state(ipmi_entity_t *entity)
{


 return utca_get_hot_swap_state(entity, hot_swap_checker, NULL);
}

static int hot_swap_state_changed(ipmi_sensor_t *sensor, enum ipmi_event_dir_e dir, int offset, int severity, int prev_severity, void *cb_data, ipmi_event_t *event)
{
    utca_fru_t *finfo = cb_data;
    enum ipmi_hot_swap_states old_state;
    int handled = IPMI_EVENT_NOT_HANDLED;
    ipmi_entity_t *entity;

    /* We only want assertions. */
    if (dir != IPMI_ASSERTION)
    {
        return handled;
    }

    if ((offset < 0) || (offset >= 8))
    {
        /* eh? */
        return handled;
    }

    entity = ipmi_sensor_get_entity(sensor);

    old_state = finfo->hs_state;
    finfo->hs_state = offset;


//printf("\t\t\t\t\t\thot_swap_state_changed %d %d\n", old_state, offset) ;

    ipmi_entity_call_hot_swap_handlers( entity, old_state, finfo->hs_state, &event, &handled ) ;


// if ((old_state == IPMI_HOT_SWAP_NOT_PRESENT)
// || (finfo->hs_state == IPMI_HOT_SWAP_NOT_PRESENT))
// {
// /* The new state is not present, scan the mc to clear it out. */
// unsigned char ipmb_addr = finfo->minfo->ipmb_address;
// int rv;
//
// i_ipmi_entity_get(entity);
// rv = ipmi_start_ipmb_mc_scan(ipmi_entity_get_domain(entity),
// 0, ipmb_addr, ipmb_addr,
// atca_event_scan_mc_done, entity);
// if (rv)
// i_ipmi_entity_put(entity);
// }


// check at which place it should be marked
    handled = IPMI_EVENT_HANDLED;

    return handled;
}


static void utca_get_hot_swap_state_done(ipmi_sensor_t *sensor, int err, ipmi_states_t *states, void *cb_data)
{
    utca_hs_info_t *hs_info = cb_data;
    utca_fru_t *finfo = hs_info->finfo;
    int i;

    if (!sensor)
    {
        ipmi_log(IPMI_LOG_ERR_INFO, "%soem_utca.c(utca_get_hot_swap_state_done): " "Sensor went away while in progress", ENTITY_NAME(finfo->entity));

        if (hs_info->handler1)
        {
            hs_info->handler1(finfo->entity, ECANCELED, 0, hs_info->cb_data);
        }
        goto out;
    }

    if (err)
    {
        ipmi_log(IPMI_LOG_ERR_INFO, "%soem_utca.c(utca_get_hot_swap_state_done): " "Error getting sensor value: 0x%x", ENTITY_NAME(finfo->entity), err);

        ipmi_detect_entity_presence_change(ipmi_sensor_get_entity(sensor), 1) ;

        if (hs_info->handler1)
        {
            // assume board is out ?
            finfo->hs_state = 0 ;
            hs_info->handler1(finfo->entity, 0, utca_hs_to_openipmi[0], hs_info->cb_data);
//            hs_info->handler1(finfo->entity, err, 0, hs_info->cb_data);
        }
        goto out;
    }

    for (i=0; i<=7; i++)
    {
        if (ipmi_is_state_set(states, i))
        {
            break;
        }
    }

    if (i > 7)
    {
        /* No state was set? */
        ipmi_log(IPMI_LOG_ERR_INFO, "%soem_utca.c(utca_get_hot_swap_state_done): " "No valid hot-swap state set in sensor response", ENTITY_NAME(finfo->entity));

        if (hs_info->handler1)
        {
            hs_info->handler1(finfo->entity, EINVAL, 0, hs_info->cb_data);
        }
        goto out;
    }

    if (hs_info->handler1)
    {
        hs_info->handler1(finfo->entity, 0, utca_hs_to_openipmi[i], hs_info->cb_data);
    }

out:
    if (finfo->entity)
    {
        ipmi_entity_opq_done(finfo->entity);
    }

    ipmi_mem_free(hs_info);
}

static void utca_get_hot_swap_state_start(ipmi_entity_t *entity, int err, void *cb_data)
{
    utca_hs_info_t *hs_info = cb_data;
    utca_fru_t *finfo = hs_info->finfo;
    int rv;

    if (err)
    {
        ipmi_log(IPMI_LOG_ERR_INFO, "%soem_utca.c(utca_get_hot_swap_state_start): " "Error in callback: 0x%x", ENTITY_NAME(entity), err);

        if (hs_info->handler1)
        {
            hs_info->handler1(entity, err, 0, hs_info->cb_data);
        }
        ipmi_entity_opq_done(entity);
        ipmi_mem_free(hs_info);
        return;
    }

    if (ipmi_sensor_id_is_invalid(&finfo->hs_sensor_id))
    {
        /* The sensor is not present, so the device is not present.
        Just return our current state. */
        finfo->hs_state = 0 ;

        if (hs_info->handler1)
        {
            hs_info->handler1(entity, 0, finfo->hs_state, hs_info->cb_data);
        }
        ipmi_entity_opq_done(entity);
        ipmi_mem_free(hs_info);
        return;
    }


    rv = ipmi_sensor_id_get_states(finfo->hs_sensor_id, utca_get_hot_swap_state_done, hs_info ) ;
    if (rv)
    {
        ipmi_log(IPMI_LOG_ERR_INFO, "%soem_utca.c(utca_get_hot_swap_state_start): " "Error sending states get: 0x%x", ENTITY_NAME(entity), rv);
        if (hs_info->handler1)
        {
            hs_info->handler1(entity, rv, 0, hs_info->cb_data);
        }
        ipmi_entity_opq_done(entity);
        ipmi_mem_free(hs_info);
    }
}

static int utca_get_hot_swap_state(ipmi_entity_t *entity, ipmi_entity_hot_swap_state_cb handler, void *cb_data)
{
    utca_hs_info_t *hs_info;
    int rv;

    hs_info = ipmi_mem_alloc(sizeof(*hs_info));

    if (!hs_info)
    {
        return ENOMEM;
    }

//printf("zawolanie get hs state\n") ;

    memset(hs_info, 0, sizeof(*hs_info));

    hs_info->handler1 = handler;
    hs_info->cb_data = cb_data;
    hs_info->finfo = ipmi_entity_get_oem_info(entity);
    rv = ipmi_entity_add_opq(entity, utca_get_hot_swap_state_start, &(hs_info->sdata), hs_info);

    if (rv)
    {
        ipmi_mem_free(hs_info);
    }
    return rv;
}


static void fetched_hot_swap_state(ipmi_sensor_t *sensor, int err, ipmi_states_t *states, void *cb_data)
{

    utca_fru_t *finfo = cb_data;
    int i;
    int handled;
    ipmi_event_t *event = NULL;
    enum ipmi_hot_swap_states old_state;

    if (err)
    {
        ipmi_detect_entity_presence_change(ipmi_sensor_get_entity(sensor), 1) ;

        ipmi_log(IPMI_LOG_WARNING, "%soem_utca.c(fetched_hot_swap_state): " "Error getting sensor value: 0x%x", SENSOR_NAME(sensor), err);
        goto out;
    }

    for (i=0; i<8; i++)
    {
        if (ipmi_is_state_set(states, i))
            break;
    }

    if (i == 8)
    {
        /* No state set, just give up. */
        ipmi_log(IPMI_LOG_WARNING, "%soem_utca.c(fetched_hot_swap_state): " "hot-swap sensor value had no valid bit set: 0x%x", SENSOR_NAME(sensor), err);
        goto out;
    }

    /* The OpenIPMI hot-swap states map directly to the ATCA ones. */
    old_state = finfo->hs_state;
    finfo->hs_state = i;


// handled = IPMI_EVENT_NOT_HANDLED;
    ipmi_entity_call_hot_swap_handlers(ipmi_sensor_get_entity(sensor), old_state, finfo->hs_state, &event, &handled);

out:
    return ;
}

static void setup_fru_hot_swap(utca_fru_t *finfo, ipmi_sensor_t *sensor)
{
    int rv = 0 ;

    finfo->hs_sensor_id = ipmi_sensor_convert_to_id(sensor);
    finfo->hs_state = 1 ; // we assume M1 state until real read is done
    finfo->presence_check_in_progress = 0 ;

    ipmi_entity_set_hot_swappable(finfo->entity, 1);
    ipmi_entity_set_supports_managed_hot_swap(finfo->entity, 1);
    ipmi_entity_set_hot_swap_control(finfo->entity, &utca_hot_swap_handlers);

    rv = ipmi_sensor_add_discrete_event_handler(sensor, hot_swap_state_changed, finfo);
    if (rv)
    {
        ipmi_log(IPMI_LOG_SEVERE, "%soem_utca.c(setup_fru_hot_swap): " "Cannot set event handler for hot-swap sensor: 0x%x", SENSOR_NAME(sensor), rv);
    }

    rv = ipmi_sensor_get_states(sensor, fetched_hot_swap_state, finfo);

    if (rv)
    {
        ipmi_log(IPMI_LOG_SEVERE, "%soem_utca.c(setup_fru_hot_swap): " "Cannot fetch current hot-swap state: 0x%x", SENSOR_NAME(sensor), rv);
    }
}

void utca_sdr_fixup(ipmi_domain_t *domain, ipmi_sdr_info_t *sdrs, void *cb_data)
{
// we go through all sdrs and correct fru id for all rtms found based on entity and instance ids
    int rv ;
    int count ;
    ipmi_sdr_t sdr;
    unsigned int i ;

    rv = ipmi_get_sdr_count(sdrs, &count);

    if (rv)
    {
        return;
    }

    for ( i = 0 ; i < count ; i++ )
    {
        rv = ipmi_get_sdr_by_index(sdrs, i, &sdr);

        if (rv)
        {
            break;
        }

        switch (sdr.type)
        {
            case IPMI_SDR_FRU_DEVICE_LOCATOR_RECORD:
            {
                if ( sdr.data[7] == 192 )
                {
                    sdr.data[1] = sdr.data[8] - 0x60 + 89 ;
                }
                ipmi_set_sdr_by_index(sdrs, i, &sdr);

                break;
            }
            default:
                continue;
        }
    }
}


//typedef void (*ipmi_sel_new_event_handler_cb)(ipmi_sel_info_t *sel,
// ipmi_mc_t *mc,
// ipmi_event_t *event,
// void *cb_data);
//int ipmi_sel_set_new_event_handler(ipmi_sel_info_t *sel,
// ipmi_sel_new_event_handler_cb handler,
// void *cb_data);

static void utca_fix_sel_handler(enum ipmi_update_e op, ipmi_domain_t *domain, ipmi_mc_t *mc, void *cb_data)
{
// char name[256] ;
//
// ipmi_mc_get_name( mc, name, 256 ) ;
//
// switch (op)
// {
// case IPMI_ADDED:
// break;
//
// default:
// break;
// }
// printf("jestem sel handlerem\n") ;
}

void utca_register_fixups(void)
{
}

static int utca_entity_presence_detect(ipmi_entity_t *entity, void *handler_data)
{
  utca_fru_t *finfo = ipmi_entity_get_oem_info(entity);


  finfo->presence_check_in_progress = 1 ;

  return 0 ;
}

static void utca_sensor_update_handler(enum ipmi_update_e op, ipmi_entity_t *entity, ipmi_sensor_t *sensor, void *cb_data)
{
    utca_fru_t *finfo = ipmi_entity_get_oem_info(entity);
    int id = ipmi_entity_get_entity_id ( entity ) ;

    if (ipmi_sensor_get_sensor_type(sensor) != 0xf0)
    {
        return;
    }


    switch (op)
    {
        case IPMI_ADDED:
        {
            setup_fru_hot_swap(finfo, sensor) ;

            // since we have hot swap sensor, we can install our presence handler to detect chages in the system
            ipmi_entity_set_presence_detector(entity, utca_entity_presence_detect, NULL ) ;
            break;
        }
        default:
        {
            break;
        }
    }
}

void utca_ent_handler(enum ipmi_update_e op, ipmi_domain_t *domain, ipmi_entity_t *entity, void *cb_data)
{
    char name[256] ;
    char fruname[256] ;

    utca_fru_t *finfo = ipmi_entity_get_oem_info(entity);

    int id = ipmi_entity_get_entity_id ( entity ) ;

// we only care about AMCs and RTMs

    if (( id != 192 ) && ( id != 193 ) && ( id != 30 ) && ( id != 10 ))
    {
        return ;
    }
    switch (op)
    {
        case IPMI_ADDED:
        {
            ipmi_entity_add_sensor_update_handler(entity, utca_sensor_update_handler, NULL) ;
            if ( finfo == NULL )
            {
                finfo = ipmi_mem_alloc( sizeof(utca_fru_t) ) ;
                finfo->entity = entity ;
                finfo->fru_id = ipmi_entity_get_fru_device_id( entity ) ;
                ipmi_entity_set_oem_info( entity, finfo, NULL ) ;
            }
        }
        break;

        case IPMI_CHANGED:
        {
            if ( finfo == NULL )
            {
                finfo = ipmi_mem_alloc( sizeof(utca_fru_t) ) ;
                finfo->entity = entity ;
                finfo->fru_id = ipmi_entity_get_fru_device_id( entity ) ;
                ipmi_entity_set_oem_info( entity, finfo, NULL ) ;
            }
            finfo->entity = entity ;
        }
        break ;

        default:
            break;
    }

}

static int check_if_utca(ipmi_domain_t* domain, ipmi_domain_oem_check_done done, void* cb_data)
{
    int rv;

    ipmi_domain_add_entity_update_handler( domain, utca_ent_handler, NULL ) ;
    ipmi_domain_set_sdrs_fixup_handler( domain, utca_sdr_fixup, NULL) ;

// int ipmi_sel_set_new_event_handler(ipmi_sel_info_t *sel, utca_sel_new_event_handler_cb, NULL )

    rv = ipmi_domain_add_event_handler(domain, utca_event_handler, NULL);
    if (rv)
    {
        ipmi_log(IPMI_LOG_SEVERE, "oem_utca.c(set_up_utca_domain): " "Could not register for events: 0x%x", rv) ;
        done(domain, rv, cb_data);

        goto out;
    }

// report success
    done(domain, 0, cb_data);

out:
    return 0 ;
}

int ipmi_oem_utca_init(void)
{
    int rv;

    if (utca_initialized)
    {
        return 0;
    }

    rv = ipmi_register_domain_oem_check(check_if_utca, NULL) ;

    if (rv)
    {
        printf("OEM install failed\n") ;
        return rv;
    }

    utca_register_fixups();

    utca_initialized = 1;

    return 0;
}

void ipmi_oem_utca_shutdown(void)
{
    if ( utca_initialized )
    {
        ipmi_deregister_domain_oem_check(check_if_utca,NULL);
        utca_initialized = 0 ;
    }
}

static void
utca_activate_done(ipmi_sensor_t *sensor,
                   int           err,
                   ipmi_msg_t    *rsp,
                   void          *cb_data)
{
    utca_hs_info_t *hs_info = cb_data;
    utca_fru_t     *finfo = hs_info->finfo;

    if (!sensor)
    {
        ipmi_log(IPMI_LOG_ERR_INFO,
                 "%soem_utca.c(utca_activate_done): "
                 "Sensor went away while in progress",
                 ENTITY_NAME(finfo->entity));
        if (hs_info->handler2)
            hs_info->handler2(finfo->entity, ECANCELED, hs_info->cb_data);
        goto out;
    }

    if (err)
    {
        ipmi_log(IPMI_LOG_ERR_INFO,
                 "%soem_utca.c(utca_activate_done): "
                 "Error setting activation: 0x%x",
                 ENTITY_NAME(finfo->entity), err);
        if (hs_info->handler2)
            hs_info->handler2(finfo->entity, err, hs_info->cb_data);
        goto out;
    }

    if (hs_info->handler2)
        hs_info->handler2(finfo->entity, 0, hs_info->cb_data);

out:
    if (sensor)
        ipmi_sensor_opq_done(sensor);
    /* There may be a destruction race condition.  I don't think so,
       though, because this is called at sensor destruction, and the
       entity should still be there. */
    if (finfo->entity)
        ipmi_entity_opq_done(finfo->entity);
    ipmi_mem_free(hs_info);
}


static void
utca_activate_sensor_start(ipmi_sensor_t *sensor, int err, void *cb_data)
{
    utca_hs_info_t *hs_info = cb_data;
    utca_fru_t     *finfo = hs_info->finfo;
    int            rv;
    ipmi_mc_t      *mc = ipmi_sensor_get_mc(sensor);
    ipmi_msg_t     msg;
    unsigned char  data[4];

    if (err)
    {
        ipmi_log(IPMI_LOG_ERR_INFO,
                 "%soem_utca.c(utca_activate_sensor_start): "
                 "Error in callback: 0x%x",
                 ENTITY_NAME(finfo->entity), err);
        if (hs_info->handler2)
            hs_info->handler2(finfo->entity, err, hs_info->cb_data);
        if (sensor)
            ipmi_sensor_opq_done(sensor);
        if (finfo->entity)
            ipmi_entity_opq_done(finfo->entity);
        ipmi_mem_free(hs_info);
        return;
    }


    msg.netfn = IPMI_GROUP_EXTENSION_NETFN;
    msg.data = data;
    data[0] = IPMI_PICMG_GRP_EXT;
    data[1] = finfo->fru_id;
    if (hs_info->op == 0x100)
    {
        msg.cmd = IPMI_PICMG_CMD_SET_FRU_ACTIVATION_POLICY;
        data[2] = 0x01; /* Enable setting the locked bit. */
        data[3] = 0x00; /* Clear the locked bit. */
        msg.data_len = 4;
    }
    else
    {
        msg.cmd = IPMI_PICMG_CMD_SET_FRU_ACTIVATION;
        data[2] = hs_info->op;
        msg.data_len = 3;
    }
    rv = ipmi_sensor_send_command(sensor, mc, 0, &msg, utca_activate_done,
                                  &hs_info->sdata2, hs_info);
    if (rv)
    {
        ipmi_log(IPMI_LOG_ERR_INFO,
                 "%soem_utca.c(utca_activate_start): "
                 "Error adding to sensor opq: 0x%x",
                 ENTITY_NAME(finfo->entity), rv);
        if (hs_info->handler2)
            hs_info->handler2(finfo->entity, rv, hs_info->cb_data);
        ipmi_sensor_opq_done(sensor);
        ipmi_entity_opq_done(finfo->entity);
        ipmi_mem_free(hs_info);
    }
}


static void utca_activate_start(ipmi_entity_t *entity, int err, void *cb_data)
{
    utca_hs_info_t *hs_info = cb_data;
    utca_fru_t     *finfo = hs_info->finfo;
    int            rv;

    if (err)
    {
        ipmi_log(IPMI_LOG_ERR_INFO,
                 "%soem_utca.c(utca_activate_start): "
                 "Error in callback: 0x%x",
                 ENTITY_NAME(entity), err);
        if (hs_info->handler2)
            hs_info->handler2(entity, err, hs_info->cb_data);
        ipmi_entity_opq_done(entity);
        ipmi_mem_free(hs_info);
        return;
    }

    if (ipmi_sensor_id_is_invalid(&finfo->hs_sensor_id))
    {
        /* The sensor is not present, so the device is not present.
           Just return our current state. */
        if (hs_info->handler2)
            hs_info->handler2(entity, EINVAL, hs_info->cb_data);
        ipmi_entity_opq_done(entity);
        ipmi_mem_free(hs_info);
        return;
    }

    rv = ipmi_sensor_id_add_opq(finfo->hs_sensor_id,
                                utca_activate_sensor_start,
                                &hs_info->sdata2,
                                hs_info);
    if (rv)
    {
        ipmi_log(IPMI_LOG_ERR_INFO,
                 "%soem_utca.c(utca_activate_start): "
                 "Error adding to sensor opq: 0x%x",
                 ENTITY_NAME(entity), rv);
        if (hs_info->handler2)
            hs_info->handler2(entity, rv, hs_info->cb_data);
        ipmi_entity_opq_done(entity);
        ipmi_mem_free(hs_info);
    }
}

static int utca_deactivate(ipmi_entity_t  *entity, ipmi_entity_cb done, void *cb_data)
{
    utca_hs_info_t *hs_info;
    int            rv;

    hs_info = ipmi_mem_alloc(sizeof(*hs_info));
    if (!hs_info) return ENOMEM;
    memset(hs_info, 0, sizeof(*hs_info));

    hs_info->handler2 = done;
    hs_info->cb_data = cb_data;
    hs_info->finfo = ipmi_entity_get_oem_info(entity);
    hs_info->op = 0; /* Do a deactivation */
    rv = ipmi_entity_add_opq(entity, utca_activate_start,  &(hs_info->sdata), hs_info);

    if (rv) ipmi_mem_free(hs_info);
    return rv;
}

static int utca_activate(ipmi_entity_t  *entity, ipmi_entity_cb done, void *cb_data)
{
    utca_hs_info_t *hs_info;
    int            rv;

    hs_info = ipmi_mem_alloc(sizeof(*hs_info));
    if (!hs_info) return ENOMEM;

    memset(hs_info, 0, sizeof(*hs_info));

    hs_info->handler2 = done;
    hs_info->cb_data = cb_data;
    hs_info->finfo = ipmi_entity_get_oem_info(entity);
    hs_info->op = 1; /* Do an activation */
    rv = ipmi_entity_add_opq(entity, utca_activate_start, &(hs_info->sdata), hs_info);

    if (rv) ipmi_mem_free(hs_info);
    return rv;
}
