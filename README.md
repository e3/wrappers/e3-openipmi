# e3-openipmi  

This module is required by `ipmiManager`.

## Requirements

- `glib2-devel`

## Installation

```sh
$ make init patch build
$ make install
```

For further targets, type `make`.

## Usage

```sh
$ iocsh.bash -r openipmi
```

## Contributing

Contributions through pull/merge requests only.
