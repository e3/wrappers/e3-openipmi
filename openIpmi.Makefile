
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile


EXCLUDE_ARCHS += linux-corei7-poky
EXCLUDE_ARCHS += linux-ppc64e6500

GLIB_LIBS   := $(shell pkg-config --libs   glib-2.0)
GLIB_CFLAGS := $(shell pkg-config --cflags glib-2.0)

GDBM_LIBS   = $(shell grep -e 'GDBM_LIB' ../config.log | sed "s/GDBM_LIB=//;s/'//g")
GDBM_CFLAGS =

USR_CFLAGS += -I../include -I..
USR_CFLAGS += $(GLIB_CFLAGS)
USR_CFLAGS += $(GDBM_CFLAGS)
USR_CFLAGS += -DIPMI_CHECK_LOCKS

LDLIBS += -lcrypto
LDLIBS += $(GLIB_LIBS)
LDLIBS += $(GDBM_LIBS)

HEADERS += include/OpenIPMI/ipmi_user.h
HEADERS += include/OpenIPMI/ipmi_types.h
HEADERS += include/OpenIPMI/ipmi_log.h
HEADERS += include/OpenIPMI/ipmi_ui.h
HEADERS += include/OpenIPMI/ipmi_picmg.h
HEADERS += include/OpenIPMI/ipmi_cmdlang.h
HEADERS += include/OpenIPMI/ipmi_lanparm.h
HEADERS += include/OpenIPMI/ipmi_bits.h
HEADERS += include/OpenIPMI/mxp.h
HEADERS += include/OpenIPMI/ipmi_addr.h
HEADERS += include/OpenIPMI/ipmi_debug.h
HEADERS += include/OpenIPMI/ipmi_fru.h
HEADERS += include/OpenIPMI/ipmi_sol.h
HEADERS += include/OpenIPMI/os_handler.h
HEADERS += include/OpenIPMI/ipmi_msgbits.h
HEADERS += include/OpenIPMI/ipmi_smi.h
HEADERS += include/OpenIPMI/ipmi_auth.h
HEADERS += include/OpenIPMI/ipmi_glib.h
HEADERS += include/OpenIPMI/ipmi_posix.h
HEADERS += include/OpenIPMI/ipmi_lan.h
HEADERS += include/OpenIPMI/ipmi_pet.h
HEADERS += include/OpenIPMI/ipmi_mc.h
HEADERS += include/OpenIPMI/ipmi_string.h
HEADERS += include/OpenIPMI/selector.h
HEADERS += include/OpenIPMI/ipmi_sdr.h
HEADERS += include/OpenIPMI/ipmi_conn.h
HEADERS += include/OpenIPMI/ipmiif.h
HEADERS += include/OpenIPMI/deprecator.h
HEADERS += include/OpenIPMI/ipmi_solparm.h
HEADERS += include/OpenIPMI/ipmi_pef.h
HEADERS += include/OpenIPMI/ipmi_err.h

KEEP_HEADER_SUBDIRS += include

SOURCES += lib/entity.c
SOURCES += lib/lanparm.c
SOURCES += lib/fru_spd_decode.c
SOURCES += lib/rakp.c
SOURCES += lib/mc.c
SOURCES += lib/md5.c
SOURCES += lib/fru.c
SOURCES += lib/pef.c
SOURCES += lib/normal_fru.c
SOURCES += lib/hmac.c
SOURCES += lib/ipmi_utils.c
SOURCES += lib/ipmi_lan.c
SOURCES += lib/oem_intel.c
SOURCES += lib/chassis.c
SOURCES += lib/oem_atca.c
SOURCES += lib/strings.c
SOURCES += lib/pet.c
SOURCES += lib/sel.c
SOURCES += lib/oem_atca_fru.c
SOURCES += lib/conn.c
SOURCES += lib/solparm.c
SOURCES += lib/domain.c
SOURCES += lib/ipmi.c
SOURCES += lib/ipmi_sol.c
SOURCES += lib/opq.c
SOURCES += lib/oem_test.c
SOURCES += lib/sdr.c
SOURCES += lib/sensor.c
SOURCES += lib/ipmi_smi.c
SOURCES += lib/oem_motorola_mxp.c
SOURCES += lib/event.c
SOURCES += lib/control.c
SOURCES += lib/oem_force_conn.c
SOURCES += lib/oem_kontron_conn.c
SOURCES += lib/aes_cbc.c
SOURCES += lib/ipmi_payload.c
SOURCES += lib/oem_atca_conn.c
SOURCES += unix/selector.c
SOURCES += unix/posix_thread_os_hnd.c
SOURCES += unix/posix_os_hnd.c
SOURCES += glib/glib_os_hnd.c
SOURCES += utils/ipmi_malloc.c
SOURCES += utils/utils_md5.c
SOURCES += utils/string.c
SOURCES += utils/locked_list.c
SOURCES += utils/md2.c
SOURCES += utils/ilist.c
SOURCES += utils/ipmi_auth.c
SOURCES += utils/hash.c
SOURCES += utils/locks.c
SOURCES += utils/os_handler.c

SOURCES += ../utcaPlugin/oem_utca.c

prebuild: config.h

config.h: bootstrap
	./bootstrap
	./configure --with-swig=no --with-tcl=no

.PHONY: vlibs 
vlibs:
